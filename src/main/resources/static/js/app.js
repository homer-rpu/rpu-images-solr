/**
 * Created by pierre on 23/10/17.
 */
var app = angular.module('ImageApp', ['ngRoute','ngResource']);
app.config(function($routeProvider){
    $routeProvider
        .when('/',{
            templateUrl: '/views/images2.html',
            controller: 'imagesController'
        })
        .otherwise(
            { redirectTo: '/'}
        );
});
