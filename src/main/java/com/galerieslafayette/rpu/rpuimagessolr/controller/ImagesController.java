package com.galerieslafayette.rpu.rpuimagessolr.controller;

import com.galerieslafayette.rpu.rpuimagessolr.service.ImagesService;
import com.galerieslafayette.rpu.rpuimagessolr.solr.model.Image;
import com.galerieslafayette.rpu.rpuimagessolr.solr.model.ImageCriteria;
import com.galerieslafayette.rpu.rpuimagessolr.solr.services.SolrImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.solr.core.query.result.FacetPage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Created by pierre on 21/10/17.
 */
@RestController
@RequestMapping("/images")
public class ImagesController {

    @Autowired
    private ImagesService imagesService;

    @Autowired
    private SolrImageService solrImageService;

    @RequestMapping("/home")
    public String home() {
        return "rpu-images-solr App Home";
    }

    @RequestMapping("/init-data")
    public void init() {
        try {
            imagesService.initData();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping("/facets")
    public ResponseEntity allFacets() {
        final FacetPage<Image> images = imagesService.allFacets();
        return new ResponseEntity(images, HttpStatus.OK);
    }

    @RequestMapping(value = "/facet/{name}/{value}", method = RequestMethod.GET)
    public ResponseEntity specificFacet(@PathVariable("name") String name,
                                        @PathVariable("value") String value) {
        final FacetPage<Image> facetsFromUnivers = imagesService.getFacetsFromUnivers(value);
        return new ResponseEntity(facetsFromUnivers, HttpStatus.OK);
    }

    @RequestMapping(value = "/find-by-criterias", method = RequestMethod.POST)
    public ResponseEntity findByCriterias(@RequestBody List<ImageCriteria> imageCriterias) {
        final FacetPage<Image> imageByCriterias = solrImageService.getImageByCriterias(imageCriterias);
        return new ResponseEntity(imageByCriterias, HttpStatus.OK);
    }


}
