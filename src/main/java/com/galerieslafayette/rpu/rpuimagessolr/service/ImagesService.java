package com.galerieslafayette.rpu.rpuimagessolr.service;

import com.galerieslafayette.rpu.rpuimagessolr.solr.model.Image;
import com.galerieslafayette.rpu.rpuimagessolr.solr.repositories.ImageRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.solr.core.query.Criteria;
import org.springframework.data.solr.core.query.result.FacetPage;
import org.springframework.data.solr.core.query.result.SolrResultPage;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by pierre on 21/10/17.
 */
@Service
public class ImagesService {

    private final ImageRepository imageRepository;

    @Value("${init.data}")
    String sampleDir;

    /**
     *
     * @return all facets with corresponding images from solr images cor
     */
    public FacetPage<Image> allFacets(){
        final FacetPage<Image> allWithFacets = imageRepository.findAllByUniversAndFamilleAndSousFamilleAndSousSousFamilleAndMarque(new PageRequest(1,50));
        return allWithFacets;
    }

    /**
     *
     * @param univers
     * @return all facets with corresponding images from solr images cor for a very one univers name
     */
    public FacetPage<Image> getFacetsFromUnivers(String univers){
        if(StringUtils.isNotBlank(univers)) {
            final FacetPage<Image> facetbyUnivers = imageRepository.findByUnivers(univers, new PageRequest(1, 50));
            return facetbyUnivers;
        }
        return null;
    }

    public ImagesService(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    public void initData() throws IOException {
        if (StringUtils.isNotBlank(sampleDir)) {
            final File file = new File(sampleDir);
            String separator = Pattern.quote("|");
            BufferedReader br = null;
            try {
                br = new BufferedReader(new FileReader(file));
                String line;

                while ((line = br.readLine()) != null) {
                    List<String> splittedLine = new ArrayList<String>();
                    for (String l : line.split(separator)) {
                        splittedLine.add(l);
                    }
                    createImage(splittedLine.get(3), splittedLine.get(5), splittedLine.get(6),
                            splittedLine.get(7), splittedLine.get(8),splittedLine.get(11));
                }
            } finally {
                br.close();
            }
        }

    }

    public void createImage(final String url, final String univers, final String famille, final String sousFamille
            , final String sousSousFamille, final String marque) {
        Image image = new Image();
        image.setUrl(url);
        image.setUnivers(univers);
        image.setFamille(famille);
        image.setSousFamille(sousFamille);
        image.setSousSousFamille(sousSousFamille);
        image.setMarque(marque);
        image.setImageProcessed("http://localhost:8002/" + url.split("/")[url.split("/").length - 1]);
        imageRepository.save(image);
    }


}
