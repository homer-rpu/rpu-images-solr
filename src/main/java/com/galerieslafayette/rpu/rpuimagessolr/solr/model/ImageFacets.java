package com.galerieslafayette.rpu.rpuimagessolr.solr.model;

/**
 * Created by pierre on 01/11/17.
 */
public class ImageFacets {

    public static final String[] FACETS = {"Univers", "Famille", "SousFamille", "SousSousFamille", "Marque"};
}
