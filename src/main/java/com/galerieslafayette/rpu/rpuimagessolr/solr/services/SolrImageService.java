package com.galerieslafayette.rpu.rpuimagessolr.solr.services;

import com.galerieslafayette.rpu.rpuimagessolr.solr.model.Image;
import com.galerieslafayette.rpu.rpuimagessolr.solr.model.ImageCriteria;
import com.galerieslafayette.rpu.rpuimagessolr.solr.model.ImageFacets;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.Criteria;
import org.springframework.data.solr.core.query.FacetOptions;
import org.springframework.data.solr.core.query.FacetQuery;
import org.springframework.data.solr.core.query.SimpleFacetQuery;
import org.springframework.data.solr.core.query.result.FacetPage;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by pierre on 01/11/17.
 */
@Service
public class SolrImageService {

    @Autowired
    SolrTemplate solrTemplate;

    private final List<String> allFacets = Arrays.asList(ImageFacets.FACETS);

    public FacetPage<Image> getImageByCriterias(final List<ImageCriteria> imageCriterias) {
        final Map<Criteria, List<String>> criteriaFromListOfCriterias = createCriteriaFromListOfCriterias(imageCriterias);

        final Criteria criteria = criteriaFromListOfCriterias.entrySet().iterator().next().getKey();

        FacetQuery facetQuery = new SimpleFacetQuery(criteria)
                .setFacetOptions(new FacetOptions().addFacetOnFlieldnames(allFacets));
        final FacetPage<Image> images = solrTemplate.queryForFacetPage(facetQuery, Image.class);
        return images;
    }

    private Map<Criteria, List<String>> createCriteriaFromListOfCriterias(List<ImageCriteria> imageCriterias) {
        Criteria criteria = null;
        final HashMap<Criteria, List<String>> criteriaStringHashMap = new HashMap<Criteria, List<String>>();
        List<String> facets = Lists.newArrayList(allFacets);

        for (ImageCriteria imageCriteria : imageCriterias) {
            if (criteria == null) {
                criteria = Criteria.where(imageCriteria.getFieldName()).expression(imageCriteria.getFieldValue().replaceAll(" ", "\\\\ "));
            } else {
                criteria = criteria.and(Criteria.where(imageCriteria.getFieldName()).expression(imageCriteria.getFieldValue().replaceAll(" ", "\\\\ ")));
            }
            facets.remove(imageCriteria.getFieldName());
        }

        criteriaStringHashMap.put(criteria, facets);
        return criteriaStringHashMap;
    }
}
