package com.galerieslafayette.rpu.rpuimagessolr.solr.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;

/**
 * Created by pierre on 21/10/17.
 */
public class Image {

    @Id
    @Indexed(name = "id",type = "string")
    private String id;

    @Indexed(name = "image_name",type = "string")
    private String imageName;

    @Indexed(name = "url",type = "string")
    private String url;

    @Indexed(name = "Univers",type ="string")
    private String univers;

    @Indexed(name = "Famille",type = "string")
    private String famille;

    @Indexed(name = "SousFamille",type = "string")
    private String sousFamille;

    @Indexed(name = "SousSousFamille",type = "string")
    private String sousSousFamille;

    @Indexed(name = "Marque",type = "string")
    private String marque;

    @Indexed(name = "ImageProcessed",type = "string")
    private String imageProcessed;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUnivers() {
        return univers;
    }

    public void setUnivers(String univers) {
        this.univers = univers;
    }

    public String getFamille() {
        return famille;
    }

    public void setFamille(String famille) {
        this.famille = famille;
    }

    public String getSousFamille() {
        return sousFamille;
    }

    public void setSousFamille(String sousFamille) {
        this.sousFamille = sousFamille;
    }

    public String getSousSousFamille() {
        return sousSousFamille;
    }

    public void setSousSousFamille(String sousSousFamille) {
        this.sousSousFamille = sousSousFamille;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getImageProcessed() {
        return imageProcessed;
    }

    public void setImageProcessed(String imageProcessed) {
        this.imageProcessed = imageProcessed;
    }
}
