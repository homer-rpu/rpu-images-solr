package com.galerieslafayette.rpu.rpuimagessolr.solr.repositories;

import com.galerieslafayette.rpu.rpuimagessolr.solr.model.Image;
import org.springframework.data.domain.Pageable;
import org.springframework.data.solr.core.query.result.FacetPage;
import org.springframework.data.solr.repository.Facet;
import org.springframework.data.solr.repository.Query;
import org.springframework.data.solr.repository.SolrCrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by pierre on 21/10/17.
 */
@Repository
public interface ImageRepository extends SolrCrudRepository<Image, String> {


    Iterable<Image> findAll();

    @Query(value = "*:*")
    @Facet(fields = {"Univers", "Famille", "SousFamille", "SousSousFamille", "Marque"})
    FacetPage<Image> findAllByUniversAndFamilleAndSousFamilleAndSousSousFamilleAndMarque(Pageable pageable);

    @Facet(fields = {"Famille", "SousFamille", "SousSousFamille", "Marque"})
    FacetPage<Image> findByUnivers(String univers, Pageable pageable);

}
