package com.galerieslafayette.rpu.rpuimagessolr.solr.model;

/**
 * Created by pierre on 01/11/17.
 */
public class ImageCriteria {

    private String fieldName;
    private String fieldValue;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }
}
