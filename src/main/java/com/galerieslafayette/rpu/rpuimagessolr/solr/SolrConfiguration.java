package com.galerieslafayette.rpu.rpuimagessolr.solr;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;

/**
 * Created by pierre on 21/10/17.
 */
@Configuration
@EnableSolrRepositories(basePackages = "com.galerieslafayette.rpu.rpuimagessolr.solr.repositories", multicoreSupport = true)
public class SolrConfiguration {

    @Bean
    public SolrClient solrClient(@Value("${solr.host}") final String solrHost) {
        return new HttpSolrClient(solrHost);
    }

    @Bean
    public SolrTemplate solrTemplate(SolrClient solrClient) {
        return new SolrTemplate(solrClient,"image");
    }

}
