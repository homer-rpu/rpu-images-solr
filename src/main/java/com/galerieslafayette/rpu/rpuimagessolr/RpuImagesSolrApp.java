package com.galerieslafayette.rpu.rpuimagessolr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by pierre on 21/10/17.
 */
@SpringBootApplication
public class RpuImagesSolrApp {

    public static void main(String[] args) {
        SpringApplication.run(RpuImagesSolrApp.class, args);
    }


}
