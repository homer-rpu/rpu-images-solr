package com.galerieslafayette.rpu.rpuimagessolr;

import com.galerieslafayette.rpu.rpuimagessolr.service.ImagesService;
import com.galerieslafayette.rpu.rpuimagessolr.solr.SolrConfiguration;
import com.galerieslafayette.rpu.rpuimagessolr.solr.repositories.ImageRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SolrConfiguration.class)
public class RpuImagesSolrApplicationTests {

    @Autowired
    private ImageRepository imageRepository;

    @Test
    public void contextLoads() throws Exception {
        assertNotNull(imageRepository);
    }

}
