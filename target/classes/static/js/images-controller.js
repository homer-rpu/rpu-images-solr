/**
 * Created by pierre on 23/10/17.
 */
app.controller('imagesController', function ($scope, $http, $q) {
    $scope.title = "Solr Images";


    $scope.getImages = function () {
        var deferred = $q.defer();
        $http.get('http://localhost:8080/images/facets').success(function (data) {
            deferred.resolve(data);
        }).error(deferred.reject);
        return deferred.promise;
    };

    $scope.getImagesFromFacets = function (facets) {
        var deferred = $q.defer();
        $http.post('http://localhost:8080/images/find-by-criterias', facets).success(function (response) {
            deferred.resolve(response);
        }).error(deferred.reject);
        return deferred.promise;
    };


    $scope.selectedFacets = [];
    $scope.images = {};
    $scope.facets = [];

    $scope.updateFacets = function (data) {
        initFacet = false;
        if ($scope.facets.length == 0) {
            initFacet = true;
        }
        $scope.facetsTmp = {
            facetFields: data.facetFields,
            facetDatas: data.facetResultPages
        }
        for (var i = 0; i < $scope.facetsTmp.facetFields.length; i++) {
            for (var j = 0; j < $scope.facetsTmp.facetDatas[i].content.length; j++) {
                $scope.facetsTmp.facetDatas[i].content[j].label = $scope.facetsTmp.facetDatas[i].content[j].value + " [" + $scope.facetsTmp.facetDatas[i].content[j].valueCount + "]";
            }
            facet = {
                fieldName: $scope.facetsTmp.facetFields[i].name,
                fieldData: $scope.facetsTmp.facetDatas[i].content
            };
            if (initFacet) {
                $scope.facets.push(facet);
            } else {
                for (var n = 0; n < $scope.facets.length; n++) {
                    if ($scope.facets[n].fieldName == facet.fieldName) {
                        $scope.facets[n] = facet;
                    }
                }
            }
        }
        console.log("Found " + $scope.images.length + " image(s).");
    };

    $scope.getImages().then(function (data) {
        $scope.images = data.content;
        $scope.updateFacets(data);
    });

    $scope.selectFacet = function (data) {
        var facet = {
            fieldName: data.field.name,
            fieldValue: data.value
        };
        if ($scope.selectedFacets.length == 0) {
            $scope.selectedFacets.push(facet);
        } else {
            var added = false;
            for (var n = 0; n < $scope.selectedFacets.length; n++) {
                if ($scope.selectedFacets[n].fieldName == facet.fieldName) {
                    $scope.selectedFacets[n] = facet;
                    added = true;
                }
            }
            if(!added){
                $scope.selectedFacets.push(facet);
            }
        }
        console.log($scope.selectedFacets);
        $scope.getImagesFromFacets($scope.selectedFacets).then(function (response) {
            console.log(response);
            $scope.images = response.content;
            $scope.updateFacets(response);
        })
    };

    $scope.reloadAllImages = function () {
        $scope.getImages().then(function (data) {
            $scope.images = data.content;
            $scope.selectedFacets = [];
            $scope.updateFacets(data);
        });
    };

});